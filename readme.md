## PROJECT ##

* ID: **W**i!**P**RAKTIKUM
* Contact: schepsen@web.de

## USAGE ##

Just clone it into your KIE Workbench

## CHANGELOG ##

### Wi!PRAKTIKUM 0.2, updated @ 2017-06-26 ###

* added Termin 6
* added Termin 1

### Wi!PRAKTIKUM 0.1, updated @ 2017-06-12 ###

* added Termin 5
* added Termin 2